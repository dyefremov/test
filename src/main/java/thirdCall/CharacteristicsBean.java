package thirdCall;

public class CharacteristicsBean {

    private Integer baseValueAmount;
    private Integer maxValueAmount;
    private boolean isVariableValue;

    public Integer getBaseValueAmount() {
        return baseValueAmount;
    }

    public void setBaseValueAmount(Integer baseValueAmount) {
        this.baseValueAmount = baseValueAmount;
    }

    public Integer getMaxValueAmount() {
        return maxValueAmount;
    }

    public void setMaxValueAmount(Integer maxValueAmount) {
        this.maxValueAmount = maxValueAmount;
    }

    public boolean isVariableValue() {
        return isVariableValue;
    }

    public void setIsVariableValue(boolean isVariableValue) {
        this.isVariableValue = isVariableValue;
    }
}
