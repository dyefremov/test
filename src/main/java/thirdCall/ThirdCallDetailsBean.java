package thirdCall;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class ThirdCallDetailsBean {

    private String creatorId;
    private String updaterId;
    private String createdTimestamp;
    private String updatedTimestamp;
    private String[] additionalAttributes;
    private String productDescription;
    private String defaultProductConfigurationId;
    private String[] searchKeywords;
    private ActivationCharacteristicBean activationCharacteristics;
    private ReloadCharacteristicBean reloadCharacteristics;
    private List<FulfillmentCharacteristicsBean> fulfillmentCharacteristics;
    private List<ProductFeesBean> productFees;
    private RedemptionCharacteristicsBean redemptionCharacteristics;
    private List<TermsAndConditionBean> termsAndConditions;
    private List<ProductConfigurationBean> productConfigurations;

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public String getUpdaterId() {
        return updaterId;
    }

    public void setUpdaterId(String updaterId) {
        this.updaterId = updaterId;
    }

    public String getCreatedTimestamp() {
        return createdTimestamp;
    }

    public void setCreatedTimestamp(String createdTimestamp) {
        this.createdTimestamp = createdTimestamp;
    }

    public String getUpdatedTimestamp() {
        return updatedTimestamp;
    }

    public void setUpdatedTimestamp(String updatedTimestamp) {
        this.updatedTimestamp = updatedTimestamp;
    }

    public String[] getAdditionalAttributes() {
        return additionalAttributes;
    }

    public void setAdditionalAttributes(String[] additionalAttributes) {
        this.additionalAttributes = additionalAttributes;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getDefaultProductConfigurationId() {
        return defaultProductConfigurationId;
    }

    public void setDefaultProductConfigurationId(String defaultProductConfigurationId) {
        this.defaultProductConfigurationId = defaultProductConfigurationId;
    }

    public String[] getSearchKeywords() {
        return searchKeywords;
    }

    public void setSearchKeywords(String[] searchKeywords) {
        this.searchKeywords = searchKeywords;
    }

    public ActivationCharacteristicBean getActivationCharacteristics() {
        return activationCharacteristics;
    }

    public void setActivationCharacteristics(ActivationCharacteristicBean activationCharacteristics) {
        this.activationCharacteristics = activationCharacteristics;
    }

    public ReloadCharacteristicBean getReloadCharacteristics() {
        return reloadCharacteristics;
    }

    public void setReloadCharacteristics(ReloadCharacteristicBean reloadCharacteristics) {
        this.reloadCharacteristics = reloadCharacteristics;
    }

    public List<FulfillmentCharacteristicsBean> getFulfillmentCharacteristics() {
        return fulfillmentCharacteristics;
    }

    public void setFulfillmentCharacteristics(List<FulfillmentCharacteristicsBean> fulfillmentCharacteristics) {
        this.fulfillmentCharacteristics = fulfillmentCharacteristics;
    }

    public List<ProductFeesBean> getProductFees() {
        return productFees;
    }

    public void setProductFees(List<ProductFeesBean> productFees) {
        this.productFees = productFees;
    }

    public RedemptionCharacteristicsBean getRedemptionCharacteristics() {
        return redemptionCharacteristics;
    }

    public void setRedemptionCharacteristics(RedemptionCharacteristicsBean redemptionCharacteristics) {
        this.redemptionCharacteristics = redemptionCharacteristics;
    }

    public List<TermsAndConditionBean> getTermsAndConditions() {
        return termsAndConditions;
    }

    public void setTermsAndConditions(List<TermsAndConditionBean> termsAndConditions) {
        this.termsAndConditions = termsAndConditions;
    }

    public List<ProductConfigurationBean> getProductConfigurations() {
        return productConfigurations;
    }

    public void setProductConfigurations(List<ProductConfigurationBean> productConfigurations) {
        this.productConfigurations = productConfigurations;
    }
}
