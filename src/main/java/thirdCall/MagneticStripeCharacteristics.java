package thirdCall;

public class MagneticStripeCharacteristics {

    private String[] redemptionTracks;

    public String[] getRedemptionTracks() {
        return redemptionTracks;
    }

    public void setRedemptionTracks(String[] redemptionTracks) {
        this.redemptionTracks = redemptionTracks;
    }
}
