package thirdCall;


public class BarCodeCharacteristicBean {

    private String id;
    private String barCodeType;
    private String barCodeSubType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBarCodeType() {
        return barCodeType;
    }

    public void setBarCodeType(String barCodeType) {
        this.barCodeType = barCodeType;
    }

    public String getBarCodeSubType() {
        return barCodeSubType;
    }

    public void setBarCodeSubType(String barCodeSubType) {
        this.barCodeSubType = barCodeSubType;
    }
}
