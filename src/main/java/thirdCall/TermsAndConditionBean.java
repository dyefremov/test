package thirdCall;


public class TermsAndConditionBean {

    private String id;
    private String termsAndConditions;
    private String termsAndConditionsType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTermsAndConditions() {
        return termsAndConditions;
    }

    public void setTermsAndConditions(String termsAndConditions) {
        this.termsAndConditions = termsAndConditions;
    }

    public String getTermsAndConditionsType() {
        return termsAndConditionsType;
    }

    public void setTermsAndConditionsType(String termsAndConditionsType) {
        this.termsAndConditionsType = termsAndConditionsType;
    }
}
