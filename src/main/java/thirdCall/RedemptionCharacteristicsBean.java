package thirdCall;

import java.util.List;

public class RedemptionCharacteristicsBean {

    private List<RedemptionTextsBean> redemptionTexts;
    private String[] redemptionOptions;
    private List<BarCodeCharacteristicBean> barCodeCharacteristics;
    private MagneticStripeCharacteristics magneticStripeCharacteristics;

    public List<RedemptionTextsBean> getRedemptionTexts() {
        return redemptionTexts;
    }

    public void setRedemptionTexts(List<RedemptionTextsBean> redemptionTexts) {
        this.redemptionTexts = redemptionTexts;
    }

    public String[] getRedemptionOptions() {
        return redemptionOptions;
    }

    public void setRedemptionOptions(String[] redemptionOptions) {
        this.redemptionOptions = redemptionOptions;
    }

    public List<BarCodeCharacteristicBean> getBarCodeCharacteristics() {
        return barCodeCharacteristics;
    }

    public void setBarCodeCharacteristics(List<BarCodeCharacteristicBean> barCodeCharacteristics) {
        this.barCodeCharacteristics = barCodeCharacteristics;
    }

    public MagneticStripeCharacteristics getMagneticStripeCharacteristics() {
        return magneticStripeCharacteristics;
    }

    public void setMagneticStripeCharacteristics(MagneticStripeCharacteristics magneticStripeCharacteristics) {
        this.magneticStripeCharacteristics = magneticStripeCharacteristics;
    }
}
