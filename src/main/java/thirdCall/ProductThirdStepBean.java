package thirdCall;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ProductThirdStepBean {

    private String entityId;
    private ProductSummaryBean summary;
    private ThirdCallDetailsBean details;

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public ProductSummaryBean getSummary() {
        return summary;
    }

    public void setSummary(ProductSummaryBean summary) {
        this.summary = summary;
    }

    public ThirdCallDetailsBean getDetails() {
        return details;
    }

    public void setDetails(ThirdCallDetailsBean details) {
        this.details = details;
    }
}
