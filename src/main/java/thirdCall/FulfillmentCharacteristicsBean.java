package thirdCall;

public class FulfillmentCharacteristicsBean {

    private String id;
    private Integer holdTimeSeconds;
    private String printingType;
    private String provisioningType;
    private String fulfillmentMethod;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getHoldTimeSeconds() {
        return holdTimeSeconds;
    }

    public void setHoldTimeSeconds(Integer holdTimeSeconds) {
        this.holdTimeSeconds = holdTimeSeconds;
    }

    public String getPrintingType() {
        return printingType;
    }

    public void setPrintingType(String printingType) {
        this.printingType = printingType;
    }

    public String getProvisioningType() {
        return provisioningType;
    }

    public void setProvisioningType(String provisioningType) {
        this.provisioningType = provisioningType;
    }

    public String getFulfillmentMethod() {
        return fulfillmentMethod;
    }

    public void setFulfillmentMethod(String fulfillmentMethod) {
        this.fulfillmentMethod = fulfillmentMethod;
    }
}
