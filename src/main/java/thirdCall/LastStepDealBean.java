package thirdCall;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class LastStepDealBean {

    private String entityId;
    private ProductThirdStepBean summary;
    private ProductThirdStepBean details;


}
