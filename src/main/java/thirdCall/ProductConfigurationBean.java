package thirdCall;

import java.util.List;

public class ProductConfigurationBean {

    private String configurationId;
    private String configurationName;
    private String configurationDescription;
    private Integer itemId;
    private String startDate;
    private String configurationStatus;
    private List<ProductImagesBean> productImages;

    public String getConfigurationId() {
        return configurationId;
    }

    public void setConfigurationId(String configurationId) {
        this.configurationId = configurationId;
    }

    public String getConfigurationName() {
        return configurationName;
    }

    public void setConfigurationName(String configurationName) {
        this.configurationName = configurationName;
    }

    public String getConfigurationDescription() {
        return configurationDescription;
    }

    public void setConfigurationDescription(String configurationDescription) {
        this.configurationDescription = configurationDescription;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getConfigurationStatus() {
        return configurationStatus;
    }

    public void setConfigurationStatus(String configurationStatus) {
        this.configurationStatus = configurationStatus;
    }

    public List<ProductImagesBean> getProductImages() {
        return productImages;
    }

    public void setProductImages(List<ProductImagesBean> productImages) {
        this.productImages = productImages;
    }
}
