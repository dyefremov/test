package thirdCall;

public class ActivationCharacteristicBean {

    private Integer baseValueAmount;
    private Integer maxValueAmount;
    private boolean isVariableValue;
    private String activationInstructions;

    public Integer getBaseValueAmount() {
        return baseValueAmount;
    }

    public void setBaseValueAmount(Integer baseValueAmount) {
        this.baseValueAmount = baseValueAmount;
    }

    public Integer getMaxValueAmount() {
        return maxValueAmount;
    }

    public void setMaxValueAmount(Integer maxValueAmount) {
        this.maxValueAmount = maxValueAmount;
    }

    public boolean isVariableValue() {
        return isVariableValue;
    }

    public void setIsVariableValue(boolean isVariableValue) {
        this.isVariableValue = isVariableValue;
    }

    public String getActivationInstructions() {
        return activationInstructions;
    }

    public void setActivationInstructions(String activationInstructions) {
        this.activationInstructions = activationInstructions;
    }
}
