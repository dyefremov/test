package thirdCall;

public class ReloadCharacteristicBean {

    private boolean isReloadable;

    public boolean isReloadable() {
        return isReloadable;
    }

    public void setIsReloadable(boolean isReloadable) {
        this.isReloadable = isReloadable;
    }
}
