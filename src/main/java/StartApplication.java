import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.util.Properties;

import com.google.gson.reflect.TypeToken;
import firstCall.ResultBean;
import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Request;
import secondCall.ProductBean;
import thirdCall.ProductThirdStepBean;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import org.apache.commons.lang3.ArrayUtils;

public class StartApplication {

	private static String requestorId = "37Q6543MKN0ZB4ZHS8C6ZJY9JM";
	public static final String JAVA_CACERTS_PATH = "\\lib\\security\\cacerts";
	private static final String DEV_KEY_STORE_PASS = "imsdev";
	private static final String CARCETS_KET_SORE_PASS = "changeit";

	public static void main(String[] args) throws URISyntaxException, IOException {
		initialiseProperties();
		trustToAllCertificates();
		defineCacerts();

		ResultBean resultBean1 = makeCall("https://api.sandbox.blackhawknetwork.com/productCatalogManagement/v1/productCatalogs", "GET", new TypeToken<ResultBean>() {});
		ProductBean resultBean2 = makeCall1(resultBean1.getResults().get(0).getEntityId(), "GET", new TypeToken<ProductBean>() {});
		for(String productUrl: resultBean2.getDetails().getProductIds()) {
			ProductThirdStepBean resultBean3 = makeCall2(productUrl, "GET", new TypeToken<ProductThirdStepBean>() {});
		}
	}

	public static void defineCacerts(){
		try {
			Properties properties = new Properties();
			String cacertsPath = System.getProperty("java.home") + JAVA_CACERTS_PATH;
			properties.put("javax.net.ssl.trustStore", cacertsPath);
			properties.put("javax.net.ssl.trustStorePassword", "changeit");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private static void trustToAllCertificates() {
		try {

			//--------------Working part-------------------------
			TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {return null;}
				public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {}
				public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {}
			}};

			SSLContext sslContext = SSLContext.getInstance("SSL");
			KeyStore trustStore = getTrustStore();

			TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
			trustManagerFactory.init(trustStore);

			KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
			keyManagerFactory.init(trustStore, DEV_KEY_STORE_PASS.toCharArray());

			TrustManager[] trustSelfAssignedCerts = trustManagerFactory.getTrustManagers();
			sslContext.init(keyManagerFactory.getKeyManagers(), ArrayUtils.addAll(trustAllCerts, trustSelfAssignedCerts), new SecureRandom());

			HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
			HostnameVerifier hv = (urlHostName, session) -> true;

			HttpsURLConnection.setDefaultHostnameVerifier(hv);
			SSLContext.setDefault(sslContext);

			/*TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {return null;}
				public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {}
				public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {}
			}};
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			HostnameVerifier hv = new HostnameVerifier() {
				public boolean verify(String urlHostName, SSLSession session) {return true;}
			};
			HttpsURLConnection.setDefaultHostnameVerifier(hv);

			SSLContext.setDefault(sc);*/
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void initialiseProperties() {
		Properties props = System.getProperties();
		props.setProperty("javax.net.ssl.keyStore", ClassLoader.getSystemResource("dev.keystore").getPath());
		props.setProperty("javax.net.ssl.keyStorePassword", "imsdev");
		props.setProperty("javax.net.ssl.keyStoreType", "jks");
		System.setProperties(props);
	}

	private static ResultBean makeCall(String url, String method, TypeToken token) throws IOException {
		Request req = RequestHelper.getRequest(url, null, requestorId, method);
		HttpResponse response = RequestHelper.executeRequest(req);

		return RequestHelper.parseResponse(response, token);
	}

	private static ProductBean makeCall1(String url, String method, TypeToken token) throws IOException {
		Request req = RequestHelper.getRequest(url, null, requestorId, method);
		HttpResponse response = RequestHelper.executeRequest(req);

		return RequestHelper.parseResponse1(response, token);
	}

	private static ProductThirdStepBean makeCall2(String url, String method, TypeToken token) throws IOException {
		Request req = RequestHelper.getRequest(url, null, requestorId, method);
		HttpResponse response = RequestHelper.executeRequest(req);

		return RequestHelper.parseResponse2(response, token);
	}

	private static KeyStore getTrustStore() throws IOException {
		KeyStore trustStore = null;
		try (FileInputStream stream = new FileInputStream(System.getProperty("javax.net.ssl.keyStore"))){
			trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(stream, DEV_KEY_STORE_PASS.toCharArray());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return trustStore;
	}

}


