package secondCall;

import firstCall.DealBhnBean;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ProductBean {

    private String entityId;
    private DealBhnBean summary;
    private DetailsBean details;

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public DealBhnBean getSummary() {
        return summary;
    }

    public void setSummary(DealBhnBean summary) {
        this.summary = summary;
    }

    public DetailsBean getDetails() {
        return details;
    }

    public void setDetails(DetailsBean details) {
        this.details = details;
    }
}
