package firstCall;

import firstCall.DealBhnBean;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class ResultBean {

    private List<DealBhnBean> results;
    private Integer total;

    public List<DealBhnBean> getResults() {
        return results;
    }

    public void setResults(List<DealBhnBean> results) {
        this.results = results;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }
}
