import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import firstCall.ResultBean;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import secondCall.ProductBean;
import thirdCall.ProductThirdStepBean;

public class RequestHelper {

	public static Request getRequest(String url, StringEntity requestBody,
			String requestorId, String requestType) {
		Request req = null;
		switch (requestType) {
		case "POST":
			req = Request.Post(url);
			break;
		case "PUT":
			req = Request.Put(url);
			break;
		case "DELETE":
			req = Request.Delete(url);
			break;
		case "GET":
			req = Request.Get(url);
			break;
		default:
			break;

		}
		if (requestBody != null) {
			req.body(requestBody);
		}
		req.addHeader("requestorId", requestorId)
				.addHeader("Content-Type", "application/json; charset=UTF-8")
				.addHeader("Accept-Encoding", "gzip, deflate, sdch")
				.addHeader("Host", "api.sandbox.blackhawknetwork.com")
				.addHeader("Accept", "application/json; charset=UTF-8");
		return req;
	}

	public static HttpResponse executeRequest(Request request)
			throws IOException {
		HttpHost proxyHost = new HttpHost("api.sandbox.blackhawknetwork.com", 443, "https");

		Credentials creds = new UsernamePasswordCredentials("clientapi@imshopping.com", "QCVJ97dXmXWIF7");
		return Executor.newInstance()
				.auth(new AuthScope(proxyHost), creds)
				.execute(request.viaProxy(proxyHost)).returnResponse();

	}

	public static ResultBean parseResponse(HttpResponse response, TypeToken token)
			throws JsonSyntaxException, ParseException, IOException {

		ResultBean result = null;
		if (response.getEntity() != null) {
			String jsonString = EntityUtils.toString(response.getEntity());
			Gson gson = new Gson();
			result = gson.fromJson(jsonString, token.getType());
		}

		return result;

	}


	public static ProductBean parseResponse1(HttpResponse response, TypeToken token)
			throws JsonSyntaxException, ParseException, IOException {

		ProductBean result = null;
		if (response.getEntity() != null) {
			String jsonString = EntityUtils.toString(response.getEntity());
			Gson gson = new Gson();
			result = gson.fromJson(jsonString, token.getType());
		}

		return result;

	}

	public static ProductThirdStepBean parseResponse2(HttpResponse response, TypeToken token)
			throws JsonSyntaxException, ParseException, IOException {

		ProductThirdStepBean result = null;
		if (response.getEntity() != null) {
			String jsonString = EntityUtils.toString(response.getEntity());
			Gson gson = new Gson();
			result = gson.fromJson(jsonString, token.getType());
		}

		return result;

	}

}
